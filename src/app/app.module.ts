import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HederComponent } from './heder/heder.component';
import { FooterComponent } from './footer/footer.component';
import { DirectivaComponent } from './directiva/directiva.component';
import { ClientesComponent } from './clientes/clientes.component';
import { ClienteService } from "./clientes/cliente.service";
// para hacer la conexion con el Back por memdio del Cor
import { HttpClientModule } from "@angular/common/http";
// Para asignar las rutas a los componenetes
import { RouterModule, Routes } from '@angular/router';
import { FlormComponent } from './clientes/florm.component';
//se importa el modulo para trabajar con formularios
import { FormsModule } from '@angular/forms';
import { registerLocaleData } from '@angular/common'
import localeES from '@angular/common/locales/es';
import { PaginatorComponent } from './paginator/paginator.component';
import { DetalleComponent } from './clientes/detalle/detalle.component';
import { ProveedoresComponent } from './proveedores/proveedores.component';
import { LoginComponent } from './login/login.component';
import {AuthGuardService} from "./service/auth-guard.service";
import { AuthService } from './service/auth.service';

registerLocaleData(localeES, 'es');

// Defino las rutas de cada componenete mediante un arreglo
const routes: Routes = [
  { path: 'login', component: LoginComponent, pathMatch: 'full' },
  { path: 'directiva', component: DirectivaComponent, canActivate: [AuthGuardService]},
  { path: 'clientes', component: ClientesComponent, canActivate: [AuthGuardService]},
  { path: 'clientes/page/:page', component: ClientesComponent, canActivate: [AuthGuardService]},
  { path: 'clientes/form', component: FlormComponent, canActivate: [AuthGuardService]},
  { path: 'clientes/form/:id', component: FlormComponent, canActivate: [AuthGuardService]},
  { path: 'clientes/ver/:id', component: DetalleComponent, canActivate: [AuthGuardService]},
  { path: 'proveedores', component: ProveedoresComponent, canActivate: [AuthGuardService]}

];

@NgModule({
  declarations: [
    AppComponent,
    HederComponent,
    FooterComponent,
    DirectivaComponent,
    ClientesComponent,
    FlormComponent,
    PaginatorComponent,
    DetalleComponent,
    ProveedoresComponent,
    LoginComponent,

  ],
  imports: [
    BrowserModule,
    // Registro las rutas deifidas en el arreglo routes
    RouterModule.forRoot(routes),
    // se importa el HttpClientModule para compartir datos con el back
    HttpClientModule,
    // Se importa fomrModule
    FormsModule

  ],
  providers: [ClienteService, AuthService, AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
