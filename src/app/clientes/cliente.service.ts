import { Injectable } from '@angular/core';
import { CLIENTES } from './clientes.json';
import { formatDate, DatePipe } from '@angular/common'
import localeES from '@angular/common/locales/es';
import { Cliente } from "./cliente";
import { catchError, map } from 'rxjs/operators'
import { of, Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Router } from "@angular/router";
import swal from 'sweetalert2'

@Injectable()
export class ClienteService {
  private urlEnPoint:string = 'http://localhost:8082/api/clientes';
  private httpHeaders = new HttpHeaders({'content-Type': 'application/json'})
  constructor(private http: HttpClient, private router: Router) { }

  getCLientes(page: number): Observable<any>{
      return this.http.get<Cliente[]>(this.urlEnPoint + '/page/' + page).pipe(
        map( (response: any) => {
          (response.content as Cliente[]).map(cliente  => {
            cliente.nombre = cliente.nombre.toUpperCase();
            cliente.apellido = cliente.apellido.toUpperCase();
            //cliente.email = cliente.email.toUpperCase();

            let datePipe = new DatePipe('es');
            cliente.createAt = datePipe.transform(cliente.createAt.toString(), 'EEE dd/MM/yyyy');
            // una de las formas de cambiar el formato de la fecha
            //cliente.createAt = formatDate(cliente.createAt.toString(), 'dd-MM-yyyy', 'en-US');
            return cliente;
          });
          return response;
        })
      );
  }

  create(cliente: Cliente): Observable<Cliente>{
    return this.http.post(this.urlEnPoint, cliente, {headers: this.httpHeaders}).pipe(
      map((response: any) => response.cliente as Cliente ),
      catchError (e => {
          console.error(e.error.mensaje);
          swal.fire('Error al Crear',e.error.error, 'error');
          return throwError(e);
      })
    );
  }

  getCliente(id): Observable<Cliente>{
    return this.http.get<Cliente>(`${this.urlEnPoint}/${ id }`).pipe(
      catchError (e => {
          this.router.navigate(['/clientes'])
          console.error(e.error.mensaje);
          swal.fire('Error al Consultar',e.error.mensaje, 'error');
          return throwError(e);
      })
    );
  }

  update(cliente: Cliente): Observable<Cliente>{
    return this.http.put<Cliente>(`${this.urlEnPoint}/${cliente.id}`, cliente, {headers: this.httpHeaders}).pipe(
      catchError (e => {
          console.error(e.error.mensaje);
          swal.fire('Error al Actualizar',e.error.error, 'error');
          return throwError(e);
      })
    );
  }

  delete (id: number): Observable<Cliente>{
    return this.http.delete<Cliente>(`${this.urlEnPoint}/${ id }`, {headers: this.httpHeaders}).pipe(
      catchError (e => {
          console.error(e.error.mensaje);
          swal.fire('Error al Eliminar',e.error.error, 'error');
          return throwError(e);
      })
    );
  }

  subirFoto(archivo: File, id): Observable<Cliente>{
    let formData = new FormData();
    formData.append("archivo", archivo);
    formData.append("id", id);

    return this.http.post(`${this.urlEnPoint}/upload`, formData).pipe(
      map( (response: any)=>response.cliente as Cliente),
      catchError (e => {
        console.error(e.error.mensaje);
        swal.fire('Error al Subir Foto',e.error.error, 'error');
        return throwError(e);
    })

    );
  }
}
