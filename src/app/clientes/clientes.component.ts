import { Component, OnInit } from '@angular/core';
import { Cliente } from "./cliente";
import { ClienteService } from "./cliente.service";
import swal from 'sweetalert2'
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',

})
export class ClientesComponent implements OnInit {

  clientes: Cliente[];
  paginador: any;

constructor(private clienteService: ClienteService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      let page: number = +params.get('page');
      if(!page){
        page = 0;
      }
       this.clienteService.getCLientes(page).subscribe(
         response =>{ this.clientes = response.content as Cliente[];
           this.paginador = response;
         }
       );
    })

  }
delete(cliente: Cliente): void{
  swal.fire({
  title: 'Deseas Eliminar?',
  text: `estas seguro que deseas Eliminar al cliente ${ cliente.nombre } ${ cliente.apellido } `,
  type: 'warning',
  showCancelButton: true,
  confirmButtonText: 'Si, eliminar!',
  cancelButtonText: 'No, cancelar!',
  reverseButtons: true
}).then((result) => {
  if (result.value) {
    this.clienteService.delete(cliente.id).subscribe(
      response => {
        this.clientes = this.clientes.filter(cli => cli !== cliente)
        swal.fire(
          'Cliente Eliminado!',
          `CLiente ${ cliente.nombre } fue eliminado con Exito`,
          'success'
        )
      }

    )

  }
})
}
}
